<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Product</title>
</head>
<body>
        {{ Form::open(array('url' => 'addproduct')) }}
        <form>
            <div class="container">
                <h1>เพิ่มสินค้า</h1>
                <div class="form-group">
                  <label for="nameproduct">ชื่อสินค้า</label>
                  <input type="nameproduct" class="form-control" name="nameproduct">
                </div>
                <div class="form-group">
                  <label for="amountproduct">จำนวนสินค้า</label>
                  <input type="amountproduct" class="form-control" name="amountproduct">

                </div>
                <div class="form-group">
                  <label for="typeproduct">ประเภทสินค้า</label>
                  <select multiple class="form-control" name="typeproduct">
                    <option value="1">เมาส์</option>
                    <option value="2">คีย์บอร์ด</option>
                    <option value="3">หูฟัง</option>
                    <option value="4">แผ่นร้องเมาส์</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="priceproduct">ราคา</label>
                  <input type="priceproduct" class="form-control" name="priceproduct">

                </div>
                <button type="submit" class="btn btn-primary">Add</button>

                <a class="text" href="manager">Back</a>

            </div>
              </form>

</body>
</html>
