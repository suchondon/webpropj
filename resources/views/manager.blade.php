<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<div class="container">
        <h1>Manage Products</h1>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">ชื่อสินค้า</th>
        <th scope="col">ราคา</th>
        <th scope="col">ประเภท</th>
        <th scope="col">จำนวน</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @foreach ($products as $item)
        <tr>
            <th scope="row">{{$item->p_id}}</th>
            <td>{{$item->nameproduct}}</td>
            <td>{{$item->priceproduct}}</td>
            @if ($item->typeproduct==1)
                <td>เมาส์</td>
            @endif
            @if ($item->typeproduct==2)
                <td>คีย์บอร์ด</td>
            @endif
            @if ($item->typeproduct==3)
                <td>หูฟัง</td>
            @endif
            @if ($item->typeproduct==4)
                <td>แผ่นรองเมาส์</td>
            @endif
            <td>{{$item->amountproduct}}</td>

        </tr>
        @endforeach
      </tr>
    </tbody>
  </table>
  <br><br>

  <a class="btn btn-info" href="addproduct" role="buttton">เพิ่ม</a>
  <a class="btn btn-info" href="deleteproduct" role="buttton">ลบ</a>
  <a class="btn btn-info" href="update" role="buttton">แก้ไข</a>

</div>
