<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
</head>
<body>
        {{ Form::open(array('url' => 'register')) }}

        <form>
        <div class="container">
         <h1>Regitster</h1>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email">
              </div>
              <div class="form-group col-md-6">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <label for="Name">Name</label>
              <input type="text" class="form-control" name="Name">
            </div>
            <div class="form-group">
              <label for="Address">Address</label>
                <textarea class="form-control" name="Address" rows="3"></textarea>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="Tel">Tel</label>
                <input type="text" class="form-control" name="Tel">
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="text" href="login">Login</a>
        </div>
          </form>

</body>
</html>


