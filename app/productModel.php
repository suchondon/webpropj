<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productModel extends Model
{
    protected $table = "product";
    protected $primaryKey = 'p_id';
    protected $fillable = [
        'nameproduct', 'amountproduct', 'typeproduct', 'priceproduct',
    ];
}
