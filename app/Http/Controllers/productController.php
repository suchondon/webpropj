<?php

namespace App\Http\Controllers;

use App\productModel;
use Illuminate\Http\Request;
use Input;
use Redirect;
use delete;
use DB;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = productModel::all();
        return view('manager',[
            'products' => $product
            ]);
    }

    public function deleteindex()
    {
        $product = productModel::all();
        return view('deleteproduct',[
            'products' => $product
            ]);
    }

    public function delete(Request $analyticsData)
    {
        DB::table('product')
        ->where('p_id',$analyticsData)
        ->delete();
        return Redirect::to('deleteproduct');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new productModel;
        $product->nameproduct = Input::get('nameproduct');
        $product->amountproduct = Input::get('amountproduct');
        $product->typeproduct = Input::get('typeproduct');
        $product->priceproduct = Input::get('priceproduct');
        $product->save();
        return Redirect::to('manager');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\productModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function show(productModel $productModel)
    {
        return $productModel;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\productModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function edit(productModel $productModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, productModel $productModel)
    {
        if ($product->fill($request->all())->sve()) {
            return true;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(productModel $productModel)
    {
        if($productModel->delete()){
            return true;
        }
    }
}
