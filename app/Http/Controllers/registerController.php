<?php

namespace App\Http\Controllers;
use App\User;
use Input;
use Hash;
use Redirect;

use Illuminate\Http\Request;

class registerController extends Controller
{
    public function register(){
        $user = new User;
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->name = Input::get('Name');
        $user->tel = Input::get('Tel');
        $user->address = Input::get('Address');
        $user->save();

        return Redirect::to('login');
    }
}
