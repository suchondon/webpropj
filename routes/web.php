<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('login');
});

Route::get('register', function () {
    return view('register');
});

Route::get('addproduct', function () {
    return view('addproduct');
});

Route::get('manager', function () {
    return view('manager');
});

Route::get('/deleteproduct', 'productController@deleteindex');

Route::get('manager', 'productController@index');
Route::post('register', 'registerController@register');
Route::post('login', 'loginController@doLogin');
Route::post('addproduct', 'productController@store');
//Route::get('deleteproduct/{p_id}', 'productController@delete');

