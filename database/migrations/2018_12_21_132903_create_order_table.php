<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('o_id');
            $table->string('list','255');
            $table->integer('amount');
            $table->integer('U_id')->unsigned();
            $table->foreign('U_id')->references('u_id')->on('users');
            $table->integer('P_id')->unsigned();
            $table->foreign('P_id')->references('p_id')->on('product');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
